﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace VAT_WPF_App
{
    /// <summary>
    /// Interaction logic for Portal.xaml
    /// </summary>
    public partial class Portal : Page
    {
        private ObservableCollection<VAT> selectedVats;

        public Portal()
        {
            InitializeComponent();

            selectedVats = GlobalData.selectedVats;

            listBox.ItemsSource = selectedVats;
        }
    }
}
