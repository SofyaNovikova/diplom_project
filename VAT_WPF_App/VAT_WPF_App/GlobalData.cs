﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections.ObjectModel;

namespace VAT_WPF_App
{
    public static class GlobalData
    {
        public static ObservableCollection<VAT> vatList = new ObservableCollection<VAT>();
        public static ObservableCollection<VAT> selectedVats = new ObservableCollection<VAT>();
    }
}
