﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VAT_WPF_App
{
    /// <summary>
    /// Interaction logic for AvestLogon.xaml
    /// </summary>
    public partial class AvestLogon : Page
    {
        public AvestLogon()
        {
            InitializeComponent();

            nextBtn.Click += nextBtn_Click;
        }

        void nextBtn_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Portal.xaml", UriKind.Relative));
        }
    }
}
