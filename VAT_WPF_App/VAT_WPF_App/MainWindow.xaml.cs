﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace VAT_WPF_App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ObservableCollection<VAT> vatList;

        public MainWindow()
        {
            InitializeComponent();
            SapLogonConnection sapConnection = new SapLogonConnection();
            vatList = new ObservableCollection<VAT>(sapConnection.getVATTableData());

            signBtn.Click += signBtn_Click;

            vatTable.ItemsSource = vatList;
        }

        void signBtn_Click(object sender, RoutedEventArgs e)
        {
            List<VAT> selectedVats = vatList.Where(vat => vat.selected).ToList();
        }
    }
}
