﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace VAT_WPF_App
{
    /// <summary>
    /// Interaction logic for Page2.xaml
    /// </summary>
    public partial class CheckPage : Page
    {
        private ObservableCollection<VAT> selectedVats;

        public CheckPage()
        {
            InitializeComponent();

            selectedVats = GlobalData.selectedVats;

            listBox.ItemsSource = selectedVats;

            nextBtn.Click += nextBtn_Click;
        }

        void nextBtn_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/AvestLogon.xaml", UriKind.Relative));
        }
    }
}
