﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPFunctionsOCX;
using SAPLogonCtrl;
using System.Reflection;

namespace VAT_WPF_App
{
    class SapLogonConnection
    {
        private string _functionName;
        private string _tableName;

        private SAPLogonControl _sapLogonControl;
        private Connection _sapConnection;
        private SAPFunctionsClass _sapFunctions;
        private Function _sapFunctionInterface;

        public SapLogonConnection()
        {
            _functionName = "ZVAT_GET_SIGNED_LIST";
            _tableName = "VATTABLE2";            

            _sapLogonControl = new SAPLogonControl();
            _sapConnection = null;
            _sapFunctions = new SAPFunctionsClass();
            _sapFunctionInterface = null;

            _sapLogonControl.Client = ConnectionConfig.Client;
            _sapLogonControl.User = "andron";
            _sapLogonControl.Password = "shuttlef23a7";
            _sapLogonControl.Language = "RU";
            _sapLogonControl.ApplicationServer = "172.16.1.245";
            _sapLogonControl.SystemNumber = 0;

            _sapConnection = (Connection)_sapLogonControl.NewConnection();
        }

        public List<VAT> getVATTableData()
        {
            List<VAT> vatList = new List<VAT>();
            if (_sapConnection.Logon(0, true))
            {
                _sapFunctionInterface = (Function)_sapFunctions.Add(_functionName);

                Parameter ErNum = (Parameter)_sapFunctionInterface.get_Exports("I_ERNAM");
                Parameter StatV = (Parameter)_sapFunctionInterface.get_Exports("I_STATV");

                ErNum._Value = "RUDNIK";
                StatV._Value = "1";

                if (_sapFunctionInterface.Call())
                {
                     dynamic SapResultTable = _sapFunctionInterface.Tables;
                     var table = SapResultTable[_tableName];
                     foreach (dynamic row in table.Rows)
                     {
                         VAT vat = new VAT((string)row["SENDERNUMBER"], (string)row["NAME"], (string)row["TOTALVAT"]);
                         vatList.Add(vat);
                     }
                }
            }
            return vatList;
        }
    }
}
