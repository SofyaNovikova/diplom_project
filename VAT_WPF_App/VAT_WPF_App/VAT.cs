﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VAT_WPF_App
{
    public class VAT
    {
        public string senderNumber {get; set;}
        public string senderName { get; set; }
        public double totalVAT { get; set; }
        public bool selected { get; set; }

        public VAT(string number, string name, string total)
        {
            senderNumber = number;
            senderName = name;
            totalVAT = Convert.ToDouble(total);
            selected = false;
        }
    }
}
