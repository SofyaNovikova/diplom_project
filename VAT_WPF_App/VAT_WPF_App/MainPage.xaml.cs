﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace VAT_WPF_App
{
    /// <summary>
    /// Interaction logic for MainPage.xaml
    /// </summary>
    public partial class MainPage : Page
    {
        private ObservableCollection<VAT> vatList;

        public MainPage()
        {
            InitializeComponent();

            SapLogonConnection sapConnection = new SapLogonConnection();
            vatList = new ObservableCollection<VAT>(sapConnection.getVATTableData());
            GlobalData.vatList = vatList;

            signBtn.Click += signBtn_Click;
            clearBtn.Click += clearBtn_Click;

            vatTable.ItemsSource = vatList;
        }

        void clearBtn_Click(object sender, RoutedEventArgs e)
        {
            List<VAT> tempList = new List<VAT>(vatList);
            tempList.ForEach(vat => vat.selected = false);
            vatList = new ObservableCollection<VAT>(tempList);

            vatTable.ItemsSource = vatList;
        }

        void signBtn_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<VAT> selectedVats =  new ObservableCollection<VAT>(vatList.Where(vat => vat.selected).ToList());
            GlobalData.selectedVats = selectedVats;

            this.NavigationService.Navigate(new Uri("/CheckPage.xaml",  UriKind.Relative));
        }
    }
}
